/* Copyright 2017 Jordan Miner
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

struct Node {
    ty: NodeType,
    span: Span,
}

enum NodeType {
    AssignmentExpr,
    Block,
    ExprStatement,
    FunctionCallExpr,
    FunctionDecl,
    IntegerLiteral,
    ParameterList,
    StringLiteral,
    VariableDecl,
}

#[derive(Debug, Copy, Clone)]
pub struct Span {
    // format: 0b0LLL_LLLL_SSSS_SSSS_SSSS_SSSS_SSSS_SSSS
    // format: 0b1DDD_DDDD_DDDD_DDDD_DDDD_DDDD_DDDD_DDDD
    // D = ID, L = length, S = start index
    // The highest bit is a flag bit:
    // - If set, data is an ID number used to look up the start and end
    // - If not set, data is 24 bits of index and 7 bits length
    data: u32,
}

// TODO: provide an AstBuilder that can be passed to the parser to build an AST
