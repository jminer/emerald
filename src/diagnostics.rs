/* Copyright 2017 Jordan Miner
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

use std::borrow::Cow;

use lexer::TokenType;

#[derive(Debug, Clone)]
pub enum DiagnosticKind {
    // lexer diagnostics
    UnexpectedCharacter,
    UnterminatedComment,
    UnexpectedEnd,
    UnexpectedToken(Cow<'static, [TokenType]>),
    // parser diagnostics
}

#[derive(Debug, Clone)]
pub struct Diagnostic {
    pub kind: DiagnosticKind,
    pub line: usize,
    pub column: usize,
}

impl Diagnostic {
    pub fn new(kind: DiagnosticKind, line: usize, column: usize) -> Diagnostic {
        Diagnostic {
            kind: kind,
            line: line,
            column: column,
        }
    }
}
