/* Copyright 2017 Jordan Miner
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

use diagnostics::*;

trait Visitor {
    fn visit_function_decl(&self) {

    }

    fn visit_diagnostic(&self, diag: &ParseDiagnostic) {

    }
}

struct Parser {
}

impl Parser {
    fn new() -> Self {
        Self {
        }
    }

    fn parse<V: Visitor>(&self, source: &str, visitor: V) {

    }
}
