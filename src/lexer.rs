/* Copyright 2015 Jordan Miner
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

use std::borrow::Cow;
use std::cell::RefCell;
use std::ops::Range;

use regex::Regex;

use diagnostics::*;

// TODO: remove SpanRange?
// different from Range in that this struct is Copy
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct SpanRange {
    start: usize,
    end: usize,
}

impl Into<Range<usize>> for SpanRange {
    fn into(self) -> Range<usize> {
        Range { start: self.start, end: self.end }
    }
}

impl From<Range<usize>> for SpanRange {
    fn from(range: Range<usize>) -> SpanRange {
        SpanRange { start: range.start, end: range.end }
    }
}

#[derive(Debug)]
pub struct SourceManager<'a> {
    src: &'a str,
    line_indexes: RefCell<Option<Vec<usize>>>, // index 0 is line 1's start index (which is 0)
    span_ranges: RefCell<Vec<SpanRange>>,
}

impl<'a> SourceManager<'a> {
    fn new(src: &str) -> SourceManager {
        SourceManager {
            src: src,
            line_indexes: RefCell::new(None),
            span_ranges: RefCell::new(vec![]),
        }
    }

    pub fn src_slice(&self, range: SpanRange) -> &'a str {
        let range: Range<usize> = range.into();
        &self.src[range]
    }

    fn build_line_index(&self) {
        let mut self_line_indexes = self.line_indexes.borrow_mut();
        if self_line_indexes.is_some() {
            return;
        }
        let bytes = self.src.as_bytes();
        // average line length is at least this much, right?
        let mut line_indexes = Vec::with_capacity(bytes.len() / 25);
        line_indexes.push(0);
        line_indexes.extend(bytes.iter().enumerate()
                            .filter_map(|(i, b)| if *b == b'\n' { Some(i + 1) } else { None }));
        *self_line_indexes = Some(line_indexes);
    }

    fn get_line(&self, index: usize) -> usize {
        self.build_line_index();
        let line_indexes_ref = self.line_indexes.borrow();
        let line_indexes = line_indexes_ref.as_ref().unwrap();

        (match line_indexes.binary_search(&index) {
            Ok(i) => i,
            Err(i) => i - 1,
        } + 1) // index 0 = line 1
    }

    fn get_column(&self, index: usize) -> usize {
        let bytes = self.src.as_bytes();
        bytes[..index].iter().rev().take_while(|b| **b != b'\n').count() + 1 // index 0 = col 1
    }

    //fn get_span(&self, range: SpanRange) -> Span {
    //    let len = range.end - range.start;
    //    // Test `len` first because it will usually be the one too large.
    //    if len > 0x7F || range.start > 0xFF_FFFF {
    //        let mut span_ranges = self.span_ranges.borrow_mut();
    //        let id = span_ranges.len();
    //        span_ranges.push(SpanRange { start: range.start, end: range.end });
    //        Span { data: (0x8000_0000 | id) as u32 }
    //    } else {
    //        Span { data: (range.start | len << 24) as u32 }
    //    }
    //}
    //
    //fn get_span_range(&self, span: &Span) -> SpanRange {
    //    if span.data & 0x8000_0000 > 0 {
    //        let span_ranges = self.span_ranges.borrow_mut();
    //        let span_range = span_ranges[(span.data & 0x7FFF_FFFF) as usize];
    //        SpanRange { start: span_range.start, end: span_range.end }
    //    } else {
    //        let start = span.data & 0xFF_FFFF;
    //        SpanRange { start: start as usize, end: (start + (span.data >> 24)) as usize }
    //    }
    //}
}

#[test]
fn test_line_index() {
    let src = "hi\nthere\n\nmore";
    let sm = SourceManager::new(src);
    sm.build_line_index();
    assert_eq!(sm.line_indexes.borrow().as_ref().unwrap(), &[0, 3, 9, 10]);

    let src = "\nhowdy\n\n";
    let sm = SourceManager::new(src);
    sm.build_line_index();
    assert_eq!(sm.line_indexes.borrow().as_ref().unwrap(), &[0, 1, 7, 8]);
}

#[test]
fn test_get_line() {
    let src = "hi\nthere\n\nmore";
    let sm = SourceManager::new(src);
    assert_eq!(sm.get_line(0), 1);
    assert_eq!(sm.get_line(1), 1);
    assert_eq!(sm.get_line(2), 1);
    assert_eq!(sm.get_line(3), 2);
    assert_eq!(sm.get_line(4), 2);
    assert_eq!(sm.get_line(7), 2);
    assert_eq!(sm.get_line(8), 2);
    assert_eq!(sm.get_line(9), 3);
    assert_eq!(sm.get_line(10), 4);
    assert_eq!(sm.get_line(11), 4);
    assert_eq!(sm.get_line(13), 4);

    let src = "\nhowdy\n\n";
    let sm = SourceManager::new(src);
    assert_eq!(sm.get_line(0), 1);
    assert_eq!(sm.get_line(1), 2);
    assert_eq!(sm.get_line(2), 2);
    assert_eq!(sm.get_line(6), 2);
    assert_eq!(sm.get_line(7), 3);
}

#[test]
fn test_get_column() {
    let src = "hi\nthere\n\nmore";
    let sm = SourceManager::new(src);
    assert_eq!(sm.get_column(0), 1);
    assert_eq!(sm.get_column(1), 2);
    assert_eq!(sm.get_column(2), 3);
    assert_eq!(sm.get_column(3), 1);
    assert_eq!(sm.get_column(4), 2);
    assert_eq!(sm.get_column(7), 5);
    assert_eq!(sm.get_column(8), 6);
    assert_eq!(sm.get_column(9), 1);
    assert_eq!(sm.get_column(10), 1);
    assert_eq!(sm.get_column(11), 2);
    assert_eq!(sm.get_column(13), 4);

    let src = "\nhowdy\n\n";
    let sm = SourceManager::new(src);
    assert_eq!(sm.get_column(0), 1);
    assert_eq!(sm.get_column(1), 1);
    assert_eq!(sm.get_column(2), 2);
    assert_eq!(sm.get_column(6), 6);
    assert_eq!(sm.get_column(7), 1);
    assert_eq!(sm.get_column(8), 1);
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Token {
    ty: TokenType,
    range: SpanRange,
}

impl Token {
    fn comment_contents<'a>(&self, src: &'a str) -> Cow<'a, str> {
        fn range_comment_contents(s: &str) -> Cow<str> {
            let mut contents = String::new();
            let first_regex = Regex::new(r"^\s*(\*+(\s+))?").unwrap();
            let rest_regex = Regex::new(r"^\s*(\*+)?").unwrap();
            let mut indent = None;

            for line in s.lines() {
                // Skip blank lines at the beginning.
                if indent.is_none() && line.trim().is_empty() {
                    continue;
                }
                let mut trimmed_line = line;
                if let Some(indent) = indent {
                    if let Some(captures) = rest_regex.captures(line) {
                        // Trim up to and including the star.
                        let cap_match = captures.get(0).unwrap();
                        trimmed_line = &line[cap_match.end()..line.len()];

                        // Trim whitespace after the star.
                        let index = trimmed_line.as_bytes().iter().enumerate().position(|(i, &b)|
                            !is_ascii_whitespace(b) || i >= indent
                        ).unwrap_or(trimmed_line.len());
                        trimmed_line = &trimmed_line[index..trimmed_line.len()];
                    }
                } else {
                    // Trim star and all whitespace on the first line of the commnent.
                    // Use it to determine how much whitespace to remove after the star on
                    // subsequent lines.
                    if let Some(captures) = first_regex.captures(line) {
                        let cap_match = captures.get(0).unwrap();
                        trimmed_line = &line[cap_match.end()..line.len()];

                        match captures.get(2) {
                            Some(cap_match) => indent = Some(cap_match.end() - cap_match.start()),
                            _ => indent = Some(0),
                        }
                    }
                }
                trimmed_line = trimmed_line.trim_right();

                contents.push_str(trimmed_line);
                contents.push('\n');
            }
            let trailing_whitespace = contents.as_bytes().iter()
                .rev().take_while(|&&b| is_ascii_whitespace(b)).count();
            let len = contents.len(); // borrow checker workaround
            contents.truncate(len - trailing_whitespace);
            Cow::Owned(contents)
        }

        match self.ty {
            TokenType::LineComment => Cow::Borrowed(&src[2..src.len()]),
            TokenType::RangeComment => range_comment_contents(&src[2..src.len()-2]),
            TokenType::DocComment => range_comment_contents(&src[3..src.len()-2]),
            _ => panic!(),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum TokenType {
    Ident,
    Integer,

    True,
    False,
    If,
    Else,
    While,
    Return,
    Pub,

    LParen,
    RParen,
    LBrace,
    RBrace,
    LBracket,
    RBracket,
    LAngle,
    RAngle,

    Semi,
    Comma,
    Dot,
    Equal,
    Exclaim,

    Plus,
    Minus,
    Star,
    Slash,

    EqualEqual,
    ExclaimEqual,

    PlusEqual,
    MinusEqual,
    StarEqual,
    SlashEqual,

    Pipe,
    Amp,

    PipePipe,
    AmpAmp,

    RangeComment,
    DocComment,
    LineComment,
    Whitespace,
}

impl TokenType {
    fn alphanumeric_token_from_str(s: &str) -> TokenType {
        match s {
            "true" => TokenType::True,
            "false" => TokenType::False,
            "if" => TokenType::If,
            "else" => TokenType::Else,
            "while" => TokenType::While,
            "return" => TokenType::Return,
            "pub" => TokenType::Pub,

            _ => TokenType::Ident,
        }
    }
}

fn is_ascii_whitespace(c: u8) -> bool {
    match c {
        b' ' | b'\t' | b'\r' | b'\n' => true,
        _ => false
    }
}

fn is_ascii_alphanumeric(c: u8) -> bool {
    match c {
        b'A' ... b'Z' | b'a' ... b'z' | b'0' ... b'9' | b'_' => true,
        _ => false
    }
}

fn is_ascii_digit(c: u8) -> bool {
    match c {
        b'0' ... b'9' => true,
        _ => false
    }
}

fn is_crlf(c: u8) -> bool {
    c == b'\r' || c == b'\n'
}

#[derive(Debug, Copy, Clone, PartialEq)]
enum PropertyState {
    // An property is not currently being lexed.
    None,
    // The '@' has been found, but not the open parenthesis yet.
    Start,
    // The '(' has been found, and the next token is a JSON token.
    Json,
}

#[derive(Debug, Clone)]
pub struct Lexer<'a, 'b: 'a> {
    source_manager: &'a SourceManager<'b>,
    skip_whitespace: bool,
    skip_comments: bool,

    index: usize,
    property_state: PropertyState,

    saved_state: Vec<(usize, PropertyState)>, // (index, property_state)
    token: Option<Result<Token, Box<Diagnostic>>>,
    last_doc_comment: Option<Token>,
}

impl<'a, 'b> Lexer<'a, 'b> {
    pub fn new(source_manager: &'a SourceManager<'b>) -> Lexer<'a, 'b> {
        Lexer {
            source_manager: source_manager,
            skip_whitespace: false,
            skip_comments: false,
            index: 0,
            property_state: PropertyState::None,
            saved_state: Vec::with_capacity(4),
            token: None,
            last_doc_comment: None,
        }
    }

    pub fn src(&self) -> &'b str {
        self.source_manager.src
    }

    pub fn skip_whitespace(&self) -> bool {
        self.skip_whitespace
    }

    pub fn set_skip_whitespace(&mut self, skip: bool) {
        self.skip_whitespace = skip;
    }

    pub fn skip_comments(&self) -> bool {
        self.skip_comments
    }

    pub fn set_skip_comments(&mut self, skip: bool) {
        self.skip_comments = skip;
    }

    pub fn last_doc_comment(&self) -> Option<Token> {
        self.last_doc_comment
    }

    pub fn clear_last_doc_comment(&mut self) {
        self.last_doc_comment = None;
    }

    pub fn last_doc_comment_contents(&self) -> Option<Cow<'b, str>> {
        self.last_doc_comment().map(|t|
            t.comment_contents(self.source_manager.src_slice(t.range))
        )
    }

    pub fn save(&mut self) {
        self.saved_state.push((self.index, self.property_state));
    }

    pub fn commit(&mut self) {
        self.saved_state.pop().unwrap();
    }

    pub fn restore(&mut self) {
        let state = self.saved_state.pop().unwrap();
        self.index = state.0;
        self.property_state = state.1;
    }

    pub fn expect_next_ty(&mut self, ty: TokenType) -> Result<Token, Box<Diagnostic>> {
        match self.next() {
            Some(Ok(token)) => {
                if token.ty == ty {
                    Ok(token)
                } else {
                    Err(Box::new(Diagnostic::new(
                        DiagnosticKind::UnexpectedToken(Cow::Owned(vec![ty])),
                        self.source_manager.get_line(token.range.start),
                        self.source_manager.get_column(token.range.start)
                    )))
                }
            },
            Some(err @ Err(_)) => err,
            None => Err(Box::new(Diagnostic::new(
                DiagnosticKind::UnexpectedEnd,
                self.source_manager.get_line(self.index),
                self.source_manager.get_column(self.index)
            ))),
        }
    }

    pub fn try_consume(&mut self, ty: TokenType) -> bool {
        self.save();
        let next = self.next();
        self.restore();
        match next {
            Some(Ok(token)) if token.ty == ty => {
                self.next();
                true
            },
            _ => false,
        }
    }

    pub fn expect_next(&mut self) -> Result<Token, Box<Diagnostic>> {
        match self.next() {
            Some(token @ Ok(_)) => token,
            Some(err @ Err(_)) => err,
            None => Err(Box::new(Diagnostic::new(
                DiagnosticKind::UnexpectedEnd,
                self.source_manager.get_line(self.index),
                self.source_manager.get_column(self.index)
            ))),
        }
    }

    // I knew how to implement a lexer, but I did skim this example presented as a fast lexer:
    // http://www.cs.dartmouth.edu/~mckeeman/cs48/mxcom/doc/lexInCpp.html
    // linked from the bottom of http://www.cs.dartmouth.edu/~mckeeman/cs48/mxcom/doc/Lexing.html
    // https://github.com/apache/avro/blob/eb31746cd5efd5e2c9c57780a651afaccd5cfe06/lang/java/compiler/src/main/javacc/org/apache/avro/compiler/idl/idl.jj
    // The error is boxed so that it can't enlarge the return value. It definitely
    // isn't the common case.
    fn lex(&mut self) -> Option<Result<Token, Box<Diagnostic>>> {
        let bytes = self.source_manager.src.as_bytes();

        let start = self.index;

        let token = |ty, this: &mut Lexer<'a, 'b>| Some(Ok(Token {
            ty: ty, range: (start..this.index).into()
        }));

        let c = if self.index < bytes.len() {
            bytes[self.index] // could use get_unchecked() for speed
        } else {
            return None;
        };
        self.index += 1;

        match c {
            b'(' => token(TokenType::LParen, self),
            b')' => token(TokenType::RParen, self),
            b'{' => token(TokenType::LBrace, self),
            b'}' => token(TokenType::RBrace, self),
            b'[' => token(TokenType::LBracket, self),
            b']' => token(TokenType::RBracket, self),
            b'<' => token(TokenType::LAngle, self),
            b'>' => token(TokenType::RAngle, self),
            b';' => token(TokenType::Semi, self),
            b',' => token(TokenType::Comma, self),
            b'.' => token(TokenType::Dot, self),

            b' ' | b'\t' | b'\r' | b'\n' => {
                while self.index < bytes.len() && is_ascii_whitespace(bytes[self.index]) {
                    self.index += 1;
                }
                token(TokenType::Whitespace, self)
            },

            b'A' ... b'Z' | b'a' ... b'z' | b'_' => {
                while self.index < bytes.len() && is_ascii_alphanumeric(bytes[self.index]) {
                    self.index += 1;
                }
                let token_text = &self.source_manager.src[start..self.index];
                let token_ty = TokenType::alphanumeric_token_from_str(token_text);
                token(token_ty, self)
            },

            b'0' ... b'9' => {
                while self.index < bytes.len() && is_ascii_digit(bytes[self.index]) {
                    self.index += 1;
                }
                token(TokenType::Integer, self)
            },

            b'=' => if self.index < bytes.len() {
                match bytes[self.index] {
                    b'=' => {
                        self.index += 1;
                        token(TokenType::EqualEqual, self)
                    },
                    _ => token(TokenType::Equal, self),
                }
            } else {
                token(TokenType::Equal, self)
            },
            b'!' => if self.index < bytes.len() {
                match bytes[self.index] {
                    b'=' => {
                        self.index += 1;
                        token(TokenType::ExclaimEqual, self)
                    },
                    _ => token(TokenType::Exclaim, self),
                }
            } else {
                token(TokenType::Exclaim, self)
            },
            b'+' => if self.index < bytes.len() {
                match bytes[self.index] {
                    b'=' => {
                        self.index += 1;
                        token(TokenType::PlusEqual, self)
                    },
                    _ => token(TokenType::Plus, self),
                }
            } else {
                token(TokenType::Plus, self)
            },
            b'-' => if self.index < bytes.len() {
                match bytes[self.index] {
                    b'=' => {
                        self.index += 1;
                        token(TokenType::MinusEqual, self)
                    },
                    _ => token(TokenType::Minus, self),
                }
            } else {
                token(TokenType::Minus, self)
            },
            b'*' => if self.index < bytes.len() {
                match bytes[self.index] {
                    b'=' => {
                        self.index += 1;
                        token(TokenType::StarEqual, self)
                    },
                    _ => token(TokenType::Star, self),
                }
            } else {
                token(TokenType::Star, self)
            },
            b'/' => if self.index < bytes.len() {
                match bytes[self.index] {
                    b'=' => {
                        self.index += 1;
                        token(TokenType::SlashEqual, self)
                    },
                    b'*' => {
                        self.index += 1;
                        let doc = self.index < bytes.len() && bytes[self.index] == b'*';
                        loop {
                            self.index += 1;
                            if bytes.len() - self.index < 2 {
                                return Some(Err(Box::new(Diagnostic::new(
                                    DiagnosticKind::UnterminatedComment,
                                    // It is most helpful to show where the comment starts.
                                    self.source_manager.get_line(start),
                                    self.source_manager.get_column(start)
                                ))));
                            }
                            if bytes[self.index + 1] == b'/' && bytes[self.index] == b'*' {
                                self.index += 2;
                                break;
                            }
                        }
                        token(if doc {
                            TokenType::DocComment
                        } else {
                            TokenType::RangeComment
                        }, self)
                    },
                    b'/' => {
                        while self.index < bytes.len() && !is_crlf(bytes[self.index]) {
                            self.index += 1;
                        }
                        token(TokenType::LineComment, self)
                    },
                    _ => token(TokenType::Slash, self),
                }
            } else {
                token(TokenType::Slash, self)
            },
            b'|' => if self.index < bytes.len() {
                match bytes[self.index] {
                    b'|' => {
                        self.index += 1;
                        token(TokenType::PipePipe, self)
                    },
                    _ => token(TokenType::Pipe, self),
                }
            } else {
                token(TokenType::Pipe, self)
            },
            b'&' => if self.index < bytes.len() {
                match bytes[self.index] {
                    b'&' => {
                        self.index += 1;
                        token(TokenType::AmpAmp, self)
                    },
                    _ => token(TokenType::Amp, self),
                }
            } else {
                token(TokenType::Amp, self)
            },

            _ => Some(Err(Box::new(Diagnostic::new(
                DiagnosticKind::UnexpectedCharacter,
                self.source_manager.get_line(self.index),
                self.source_manager.get_column(self.index)
            )))),
        }
    }
}

impl<'a, 'b> Iterator for Lexer<'a, 'b> {
    type Item = Result<Token, Box<Diagnostic>>;

    fn next(&mut self) -> Option<Result<Token, Box<Diagnostic>>> {
        if let Some(Err(_)) = self.token {
            return None;
        }
        loop {
            self.token = self.lex();
            if let Some(Ok(token @ Token { .. })) = self.token {
                if token.ty == TokenType::DocComment {
                    self.last_doc_comment = Some(token);
                }
                if self.skip_whitespace && token.ty == TokenType::Whitespace {
                    continue;
                }
                if self.skip_comments {
                    match token.ty {
                        TokenType::RangeComment |
                        TokenType::LineComment |
                        TokenType::DocComment => continue,
                        _ => {}
                    }
                }
            }
            break;
        }
        self.token.clone()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic_lexing() {
    let sm = SourceManager::new(
"main() {
}");
    let lexer = Lexer::new(&sm);
    let tokens = lexer.map(|r| {
        let t = r.unwrap();
        (t.ty, sm.src_slice(t.range))
    }).collect::<Vec<_>>();
    assert_eq!(&tokens[..], &[
        (TokenType::Ident, "main"),
        (TokenType::LParen, "("),
        (TokenType::RParen, ")"),
        (TokenType::Whitespace, " "),
        (TokenType::LBrace, "{"),
        (TokenType::Whitespace, "\n"),
        (TokenType::RBrace, "}")
    ]);
    }

    #[test]
    fn arithmatic_operators() {
    let sm = SourceManager::new(
"calc(a, b) {
    x = 23 * 5 + (3 - a) / b
}");
    let lexer = Lexer::new(&sm);
    let tokens = lexer.map(|r| {
        let t = r.unwrap();
        (t.ty, sm.src_slice(t.range))
    }).collect::<Vec<_>>();
    assert_eq!(&tokens[..], &[
        (TokenType::Ident, "calc"),
        (TokenType::LParen, "("),
        (TokenType::Ident, "a"),
        (TokenType::Comma, ","),
        (TokenType::Whitespace, " "),
        (TokenType::Ident, "b"),
        (TokenType::RParen, ")"),
        (TokenType::Whitespace, " "),
        (TokenType::LBrace, "{"),

        (TokenType::Whitespace, "\n    "),
        (TokenType::Ident, "x"),
        (TokenType::Whitespace, " "),
        (TokenType::Equal, "="),
        (TokenType::Whitespace, " "),
        (TokenType::Integer, "23"),
        (TokenType::Whitespace, " "),
        (TokenType::Star, "*"),
        (TokenType::Whitespace, " "),
        (TokenType::Integer, "5"),
        (TokenType::Whitespace, " "),
        (TokenType::Plus, "+"),
        (TokenType::Whitespace, " "),
        (TokenType::LParen, "("),
        (TokenType::Integer, "3"),
        (TokenType::Whitespace, " "),
        (TokenType::Minus, "-"),
        (TokenType::Whitespace, " "),
        (TokenType::Ident, "a"),
        (TokenType::RParen, ")"),
        (TokenType::Whitespace, " "),
        (TokenType::Slash, "/"),
        (TokenType::Whitespace, " "),
        (TokenType::Ident, "b"),

        (TokenType::Whitespace, "\n"),
        (TokenType::RBrace, "}")
    ][..]);
    }

    #[test]
    fn arithmatic_assign_operators() {
    let sm = SourceManager::new(
"calc(a, b) {
    x += y -= a *= b /= x
}");
    let lexer = Lexer::new(&sm);
    let tokens = lexer.map(|r| {
        let t = r.unwrap();
        (t.ty, sm.src_slice(t.range))
    }).collect::<Vec<_>>();
    assert_eq!(&tokens[..], &[
        (TokenType::Ident, "calc"),
        (TokenType::LParen, "("),
        (TokenType::Ident, "a"),
        (TokenType::Comma, ","),
        (TokenType::Whitespace, " "),
        (TokenType::Ident, "b"),
        (TokenType::RParen, ")"),
        (TokenType::Whitespace, " "),
        (TokenType::LBrace, "{"),

        (TokenType::Whitespace, "\n    "),
        (TokenType::Ident, "x"),
        (TokenType::Whitespace, " "),
        (TokenType::PlusEqual, "+="),
        (TokenType::Whitespace, " "),
        (TokenType::Ident, "y"),
        (TokenType::Whitespace, " "),
        (TokenType::MinusEqual, "-="),
        (TokenType::Whitespace, " "),
        (TokenType::Ident, "a"),
        (TokenType::Whitespace, " "),
        (TokenType::StarEqual, "*="),
        (TokenType::Whitespace, " "),
        (TokenType::Ident, "b"),
        (TokenType::Whitespace, " "),
        (TokenType::SlashEqual, "/="),
        (TokenType::Whitespace, " "),
        (TokenType::Ident, "x"),

        (TokenType::Whitespace, "\n"),
        (TokenType::RBrace, "}")
    ]);
    }

    #[test]
    fn if_else() {
    let sm = SourceManager::new(
"if(x == 2 || y) {
} else {}");
    let lexer = Lexer::new(&sm);
    let tokens = lexer.map(|r| {
        let t = r.unwrap();
        (t.ty, sm.src_slice(t.range))
    }).collect::<Vec<_>>();
    assert_eq!(&tokens[..], &[
        (TokenType::If, "if"),
        (TokenType::LParen, "("),
        (TokenType::Ident, "x"),
        (TokenType::Whitespace, " "),
        (TokenType::EqualEqual, "=="),
        (TokenType::Whitespace, " "),
        (TokenType::Integer, "2"),
        (TokenType::Whitespace, " "),
        (TokenType::PipePipe, "||"),
        (TokenType::Whitespace, " "),
        (TokenType::Ident, "y"),
        (TokenType::RParen, ")"),
        (TokenType::Whitespace, " "),
        (TokenType::LBrace, "{"),

        (TokenType::Whitespace, "\n"),
        (TokenType::RBrace, "}"),
        (TokenType::Whitespace, " "),
        (TokenType::Else, "else"),
        (TokenType::Whitespace, " "),
        (TokenType::LBrace, "{"),
        (TokenType::RBrace, "}"),
    ]);
    }
}