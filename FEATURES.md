
Definite features:

- Raw strings
- Interpolated strings
- Underscore separators in number literals

Maybe:

- Just classes
- Privacy based on a leading underscore like Dart
- Fiber support like Wren

TODO:

- Remove regex dependency. It should be easy enough and reduce binary size quite a bit, especially since regex seems to have 15 dependencies.
