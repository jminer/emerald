
Emerald's goals in order:

1. Embeddable. The entire compiler should be no more than a couple hundred KB. There are many scripting languages and many statically-typed languages, but mainly only Lua and Wren have embedding as a high priority.
2. Easy-to-use. It should be easy to learn and productive to write code in.
3. Performance. It should be possible to write code that is close to the speed of C. It should be usable for soft real-time (games and UI scripting), either with reference counting or a low-latency garbage collector.
